using System;
using SimpleIoC.Bindings.Abstractions;
using SimpleIoC.Identifiers.Abstractions;

namespace SimpleIoC.Abstractions
{
	public interface IContext
	{
		IBindingsCollection Bindings { get; }

		T Get<T>(IIdentifier identifier = null);
		object Get(Type type, IIdentifier identifier = null);
	}
}