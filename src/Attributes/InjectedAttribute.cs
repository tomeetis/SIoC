using System;
using SimpleIoC.Identifiers;
using SimpleIoC.Identifiers.Abstractions;
using SimpleIoC.Models;

namespace SimpleIoC.Attributes
{
	[AttributeUsage(
		AttributeTargets.Field,
		AllowMultiple = false,
		Inherited = false)]
	public class InjectedAttribute : Attribute
	{
		public readonly IIdentifier identifier;


		public InjectedAttribute()
			: this((IIdentifier)null)
		{ }

		public InjectedAttribute(string identifier)
			: this((IIdentifier)new StringIdentifier(identifier))
		{ }

		public InjectedAttribute(int identifier)
			: this((IIdentifier)new IntegerIdentifier(identifier))
		{ }

		private InjectedAttribute(IIdentifier identifier)
		{
			this.identifier = identifier ?? Defaults.Identifier;
		}
	}
}