using SimpleIoC.Identifiers;
using SimpleIoC.Identifiers.Abstractions;

namespace SimpleIoC.Models
{
	public static class Defaults
	{
		public static IIdentifier Identifier = new StringIdentifier("SimpleIoC_DefaultIdentifier");
	}
}