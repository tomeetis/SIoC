namespace SimpleIoC.Models
{
	public enum ObjectLifetime
	{
		Singleton,
		Transient
	}
}