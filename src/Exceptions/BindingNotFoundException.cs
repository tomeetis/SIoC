using System;
using SimpleIoC.Identifiers.Abstractions;

namespace SimpleIoC.Exceptions
{
	public sealed class BindingNotFoundException : BindingException
	{
		public BindingNotFoundException(Type bindingType)
			: base(bindingType.Name)
		{ }

		public BindingNotFoundException(Type bindingType, string customMessage)
			: base(bindingType, customMessage)
		{ }

		public BindingNotFoundException(Type bindingType, IIdentifier identifier)
			: base($"{identifier} -> {bindingType.Name}")
		{ }
	}
}