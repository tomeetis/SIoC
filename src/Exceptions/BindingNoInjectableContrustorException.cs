using System;

namespace SimpleIoC.Exceptions
{
	public sealed class BindingNoInjectableContrustorException : BindingException
	{

		public BindingNoInjectableContrustorException(Type bindingType)
			: base(bindingType.Name)
		{ }

		public BindingNoInjectableContrustorException(Type bindingType, string customMessage)
			: base(bindingType, customMessage)
		{ }
	}
}