using System;

namespace SimpleIoC.Exceptions
{
	public sealed class BindingAlreadyExistsException : BindingException
	{
		public BindingAlreadyExistsException(Type bindingType)
			: base(bindingType.Name)
		{ }

		public BindingAlreadyExistsException(Type bindingType, string customMessage)
			: base(bindingType, customMessage)
		{ }
	}
}