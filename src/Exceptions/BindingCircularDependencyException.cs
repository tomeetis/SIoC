using System.Linq;
using SimpleIoC.Dependencies;

namespace SimpleIoC.Exceptions
{
	public sealed class BindingCircularDependencyException : BindingException
	{
		public BindingCircularDependencyException(DependenciesMap map)
			: base(string.Join(" -> ", map.DependenciesPath.Select((item) => item.ToString())))
		{ }
	}
}