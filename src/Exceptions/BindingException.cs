using System;

namespace SimpleIoC.Exceptions
{
	public class BindingException : Exception
	{
		public BindingException()
		{ }

		public BindingException(string message)
			: base(message)
		{ }

		public BindingException(Type bindingType)
			: base(bindingType.Name)
		{ }

		public BindingException(Type bindingType, string message)
			: base($"{bindingType.Name} - {message}")
		{ }
	}
}