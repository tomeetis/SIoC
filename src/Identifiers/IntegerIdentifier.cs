using SimpleIoC.Identifiers.Abstractions;

namespace SimpleIoC.Identifiers
{
	public sealed class IntegerIdentifier : IIdentifier
	{
		private readonly int _identifier;


		public IntegerIdentifier(int identifier)
		{
			_identifier = identifier;
		}


		public override bool Equals(object obj)
		{
			if (obj is IntegerIdentifier intObj)
			{
				return intObj?._identifier == this._identifier;
			}

			return false;
		}

		public override int GetHashCode()
		{
			return _identifier.GetHashCode();
		}

		public object GetIdentifier()
		{
			return _identifier;
		}

		public override string ToString()
		{
			return _identifier.ToString();
		}


		public static implicit operator IntegerIdentifier(int value) => new IntegerIdentifier(value);
		public static implicit operator int(IntegerIdentifier value) => (int)value._identifier;

		public static bool operator ==(IntegerIdentifier a, IntegerIdentifier b)
		{
			return a?._identifier == b?._identifier;
		}
		public static bool operator !=(IntegerIdentifier a, IntegerIdentifier b)
		{
			return a?._identifier != b?._identifier;
		}

		public static bool operator ==(IntegerIdentifier a, int b)
		{
			return a?._identifier == b;
		}
		public static bool operator !=(IntegerIdentifier a, int b)
		{
			return a?._identifier != b;
		}

		public static bool operator ==(int a, IntegerIdentifier b)
		{
			return a == b?._identifier;
		}
		public static bool operator !=(int a, IntegerIdentifier b)
		{
			return a != b?._identifier;
		}
	}
}