namespace SimpleIoC.Identifiers.Abstractions
{
	public interface IIdentifier
	{
		object GetIdentifier();
	}
}