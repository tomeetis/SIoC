using SimpleIoC.Identifiers.Abstractions;

namespace SimpleIoC.Identifiers
{
	public sealed class StringIdentifier : IIdentifier
	{
		private readonly string _identifier;


		public StringIdentifier(string identifier)
		{
			_identifier = identifier;
		}


		public override bool Equals(object obj)
		{
			if (obj is StringIdentifier strObj)
			{
				return strObj?._identifier == this._identifier;
			}

			return false;
		}

		public override int GetHashCode()
		{
			return _identifier.GetHashCode();
		}

		public object GetIdentifier()
		{
			return _identifier;
		}

		public override string ToString()
		{
			return _identifier;
		}


		public static implicit operator StringIdentifier(string value) => new StringIdentifier(value);
		public static implicit operator string(StringIdentifier value) => (string)value._identifier;

		public static bool operator ==(StringIdentifier a, StringIdentifier b)
		{
			return a?._identifier == b?._identifier;
		}
		public static bool operator !=(StringIdentifier a, StringIdentifier b)
		{
			return a?._identifier != b?._identifier;
		}

		public static bool operator ==(StringIdentifier a, string b)
		{
			return a?._identifier == b;
		}
		public static bool operator !=(StringIdentifier a, string b)
		{
			return a?._identifier != b;
		}

		public static bool operator ==(string a, StringIdentifier b)
		{
			return a == b?._identifier;
		}
		public static bool operator !=(string a, StringIdentifier b)
		{
			return a != b?._identifier;
		}
	}
}