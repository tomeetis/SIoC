using System.Linq;
using System.Reflection;
using SimpleIoC.Abstractions;
using SimpleIoC.Attributes;

namespace SimpleIoC
{
	public static class Injector
	{
		public static void Inject(object instance, IContext context)
		{
			var fieldsToInject = GetFieldsToInject(instance);

			foreach (var field in fieldsToInject)
			{
				var fieldAttribute = (InjectedAttribute)field.GetCustomAttribute(typeof(InjectedAttribute));
				var fieldValue = context.Get(field.FieldType, fieldAttribute.identifier);

				field.SetValue(instance, fieldValue);
			}
		}


		private static FieldInfo[] GetFieldsToInject(object instance)
		{
			var allInstanceFields = instance.GetType().GetFields(
				BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);

			var attributeType = typeof(InjectedAttribute);

			var fieldsToInject = allInstanceFields.Where(field => field.IsDefined(attributeType)).ToArray();
			return fieldsToInject;
		}
	}
}