﻿using System;
using System.Runtime.CompilerServices;
using SimpleIoC.Abstractions;
using SimpleIoC.Bindings;
using SimpleIoC.Bindings.Abstractions;
using SimpleIoC.Dependencies;
using SimpleIoC.Identifiers.Abstractions;
using SimpleIoC.Models;

[assembly: InternalsVisibleTo("SimpleIoC.Tests")]
namespace SimpleIoC
{
	public class Context : IContext
	{
		private readonly DependenciesMap _circularDependenciesBlock = new DependenciesMap();


		public IBindingsCollection Bindings { get; } = new BindingsCollection();


		public T Get<T>(IIdentifier identifier = null)
		{
			return (T)Get(typeof(T), identifier);
		}

		public object Get(Type type, IIdentifier identifier = null)
		{
			_circularDependenciesBlock.Add(type);

			var bindingInfo = Bindings.Get(type, identifier);
			var resolvedObject = Resolve(bindingInfo);

			_circularDependenciesBlock.RemoveFrom(type);
			return resolvedObject;
		}


		private object CreateInstance(IBinding binding)
		{
			return binding.InstantiationFunction != default ?
				binding.InstantiationFunction() :
				Activator.CreateInstance(binding.ConcreteType);
		}

		private object Resolve(IBinding binding)
		{
			if (binding.Value != default)
			{
				return binding.Value;
			}

			var instance = CreateInstance(binding);

			if (instance != null)
			{
				Injector.Inject(instance, this);

				if (binding.Lifetime == ObjectLifetime.Singleton)
				{
					binding.SetValue(instance);
				}
			}

			return instance;
		}
	}
}
