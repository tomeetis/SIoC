using System;
using System.Collections.Generic;
using SimpleIoC.Exceptions;

namespace SimpleIoC.Dependencies
{
	public sealed class DependenciesMap
	{
		private readonly List<Type> _dependenciesPath = new List<Type>(1);


		public List<Type> DependenciesPath => new List<Type>(_dependenciesPath);


		public void Add(Type type)
		{
			if (_dependenciesPath.Contains(type))
			{
				_dependenciesPath.Add(type);
				throw new BindingCircularDependencyException(this);
			}

			_dependenciesPath.Add(type);
		}

		public void Clear()
		{
			_dependenciesPath.Clear();
			_dependenciesPath.TrimExcess();
		}

		public void RemoveFrom(Type type)
		{
			var index = _dependenciesPath.IndexOf(type);

			if (index >= 0)
			{
				_dependenciesPath.RemoveRange(index, _dependenciesPath.Count - index);
				_dependenciesPath.TrimExcess();
			}
		}
	}
}