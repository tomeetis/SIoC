using System;
using SimpleIoC.Identifiers.Abstractions;

namespace SimpleIoC.Bindings.Abstractions
{
	public interface IBindingsCollection
	{
		IBindingsCollection AddSingleton<T>(IIdentifier identifier = null);
		IBindingsCollection AddSingleton<T>(Func<object> constructorFunc, IIdentifier identifier = null);
		IBindingsCollection AddSingleton<T>(T implementation, IIdentifier identifier = null);
		IBindingsCollection AddSingleton<T, V>(IIdentifier identifier = null) where V : T;
		IBindingsCollection AddSingleton<T, V>(Func<object> constructorFunc, IIdentifier identifier = null) where V : T;
		IBindingsCollection AddSingleton<T, V>(V implementation, IIdentifier identifier = null) where V : T;

		IBindingsCollection AddSingleton(Type type, IIdentifier identifier = null);
		IBindingsCollection AddSingleton(Type type, Func<object> constructorFunc, IIdentifier identifier = null);
		IBindingsCollection AddSingleton(Type type, object implementation, IIdentifier identifier = null);
		IBindingsCollection AddSingleton(Type abstractType, Type concreteType, IIdentifier identifier = null);
		IBindingsCollection AddSingleton(Type abstractType, Type concreteType, Func<object> constructorFunc,
			IIdentifier identifier = null);
		IBindingsCollection AddSingleton(Type abstractType, Type concreteType, object implementation,
			IIdentifier identifier = null);

		IBindingsCollection AddTransient<T>(IIdentifier identifier = null);
		IBindingsCollection AddTransient<T>(Func<object> constructorFunc, IIdentifier identifier = null);
		IBindingsCollection AddTransient<T, V>(IIdentifier identifier = null) where V : T;
		IBindingsCollection AddTransient<T, V>(Func<object> constructorFunc, IIdentifier identifier = null) where V : T;

		IBindingsCollection AddTransient(Type type, IIdentifier identifier = null);
		IBindingsCollection AddTransient(Type type, Func<object> constructorFunc, IIdentifier identifier = null);
		IBindingsCollection AddTransient(Type abstractType, Type concreteType, IIdentifier identifier = null);
		IBindingsCollection AddTransient(Type abstractType, Type concreteType, Func<object> constructorFunc,
			IIdentifier identifier = null);

		IBinding Get(Type type, IIdentifier identifier = null);
	}
}