using System;
using SimpleIoC.Identifiers.Abstractions;
using SimpleIoC.Models;

namespace SimpleIoC.Bindings.Abstractions
{
	public interface IBinding
	{
		Type AbstractType { get; }
		Type ConcreteType { get; }
		IIdentifier Identifier { get; }
		Func<object> InstantiationFunction { get; }
		ObjectLifetime Lifetime { get; }
		object Value { get; }

		void SetValue(object value);
	}
}