using System;
using System.Collections.Generic;
using SimpleIoC.Bindings.Abstractions;
using SimpleIoC.Exceptions;
using SimpleIoC.Identifiers.Abstractions;
using SimpleIoC.Models;

namespace SimpleIoC.Bindings
{
	public sealed class BindingsCollection : IBindingsCollection
	{
		private Dictionary<Type, Dictionary<IIdentifier, IBinding>> _bindings;


		public BindingsCollection()
		{
			_bindings = new Dictionary<Type, Dictionary<IIdentifier, IBinding>>();
		}


		public IBindingsCollection AddSingleton<T>(IIdentifier identifier = null)
		{
			return AddSingleton(typeof(T), identifier);
		}

		public IBindingsCollection AddSingleton<T>(Func<object> constructorFunc, IIdentifier identifier = null)
		{
			return AddSingleton(typeof(T), constructorFunc, identifier);
		}

		public IBindingsCollection AddSingleton<T>(T implementation, IIdentifier identifier = null)
		{
			return AddSingleton(typeof(T), implementation, identifier);
		}

		public IBindingsCollection AddSingleton<T, V>(IIdentifier identifier = null) where V : T
		{
			return AddSingleton(typeof(T), typeof(V), identifier);
		}

		public IBindingsCollection AddSingleton<T, V>(Func<object> constructorFunc, IIdentifier identifier = null) where V : T
		{
			return AddSingleton(typeof(T), typeof(V), constructorFunc, identifier);
		}

		public IBindingsCollection AddSingleton<T, V>(V implementation, IIdentifier identifier = null) where V : T
		{
			return AddSingleton(typeof(T), typeof(V), implementation, identifier);
		}

		public IBindingsCollection AddSingleton(Type type, IIdentifier identifier = null)
		{
			AddBinding(new Binding(type, type.IsAbstract ? default : type, identifier, default,
				ObjectLifetime.Singleton, default));

			return this;
		}

		public IBindingsCollection AddSingleton(Type type, Func<object> constructorFunc, IIdentifier identifier = null)
		{
			AddBinding(new Binding(type, type.IsAbstract ? default : type, identifier, constructorFunc,
				ObjectLifetime.Singleton, default));

			return this;
		}

		public IBindingsCollection AddSingleton(Type type, object implementation, IIdentifier identifier = null)
		{
			AddBinding(new Binding(type, type.IsAbstract ? default : type, identifier, default,
							ObjectLifetime.Singleton, implementation));

			return this;
		}

		public IBindingsCollection AddSingleton(Type abstractType, Type concreteType, IIdentifier identifier = null)
		{
			FailIfSealed(abstractType);
			FailIfAbstract(concreteType);

			AddBinding(new Binding(abstractType, concreteType, identifier, default, ObjectLifetime.Singleton, default));
			return this;
		}

		public IBindingsCollection AddSingleton(Type abstractType, Type concreteType, Func<object> constructorFunc,
			IIdentifier identifier = null)
		{
			FailIfSealed(abstractType);
			FailIfAbstract(concreteType);

			AddBinding(new Binding(abstractType, concreteType, identifier, constructorFunc, ObjectLifetime.Singleton,
				default));

			return this;
		}

		public IBindingsCollection AddSingleton(Type abstractType, Type concreteType, object implementation,
			IIdentifier identifier = null)
		{
			FailIfSealed(abstractType);
			FailIfAbstract(concreteType);

			AddBinding(new Binding(abstractType, concreteType, identifier, default, ObjectLifetime.Singleton,
				implementation));

			return this;
		}


		public IBindingsCollection AddTransient<T>(IIdentifier identifier = null)
		{
			return AddTransient(typeof(T), identifier);
		}

		public IBindingsCollection AddTransient<T>(Func<object> constructorFunc, IIdentifier identifier = null)
		{
			return AddTransient(typeof(T), constructorFunc, identifier);
		}

		public IBindingsCollection AddTransient<T, V>(IIdentifier identifier = null) where V : T
		{
			return AddTransient(typeof(T), typeof(V), identifier);
		}

		public IBindingsCollection AddTransient<T, V>(Func<object> constructorFunc, IIdentifier identifier = null) where V : T
		{
			return AddTransient(typeof(T), typeof(V), constructorFunc, identifier);
		}

		public IBindingsCollection AddTransient(Type type, IIdentifier identifier = null)
		{
			AddBinding(new Binding(type, type.IsAbstract ? default : type, identifier, default,
							ObjectLifetime.Transient, default));

			return this;
		}

		public IBindingsCollection AddTransient(Type type, Func<object> constructorFunc, IIdentifier identifier = null)
		{
			AddBinding(new Binding(type, type.IsAbstract ? default : type, identifier, constructorFunc,
							ObjectLifetime.Transient, default));

			return this;
		}

		public IBindingsCollection AddTransient(Type abstractType, Type concreteType, IIdentifier identifier = null)
		{
			FailIfSealed(abstractType);
			FailIfAbstract(concreteType);

			AddBinding(new Binding(abstractType, concreteType, identifier, default, ObjectLifetime.Transient, default));

			return this;
		}

		public IBindingsCollection AddTransient(Type abstractType, Type concreteType, Func<object> constructorFunc,
			IIdentifier identifier = null)
		{
			FailIfSealed(abstractType);
			FailIfAbstract(concreteType);

			AddBinding(new Binding(abstractType, concreteType, identifier, constructorFunc, ObjectLifetime.Transient,
				default));

			return this;
		}

		public IBinding Get(Type type, IIdentifier identifier = null)
		{
			identifier = identifier ?? Defaults.Identifier;

			if (_bindings.TryGetValue(type, out var typedBindings))
			{
				if (typedBindings.TryGetValue(identifier, out var binding))
				{
					return binding;
				}
			}

			throw new BindingNotFoundException(type, identifier);
		}


		private void AddBinding(IBinding binding)
		{
			FailIfExists(binding.AbstractType, binding.Identifier);

			if (!_bindings.TryGetValue(binding.AbstractType, out var typedBindings))
			{
				typedBindings = new Dictionary<IIdentifier, IBinding>();
				_bindings.Add(binding.AbstractType, typedBindings);
			}

			if (typedBindings.ContainsKey(binding.Identifier))
			{
				throw new BindingAlreadyExistsException(binding.AbstractType);
			}
			else
			{
				typedBindings.Add(binding.Identifier, binding);
			}
		}

		private void FailIfAbstract(Type type)
		{
			if (type.IsAbstract)
			{
				throw new BindingException($"Type cannot be abstract.");
			}
		}

		private void FailIfSealed(Type type)
		{
			if (type.IsSealed)
			{
				throw new BindingException("Type cannot be sealed.");
			}
		}

		private void FailIfExists(Type type, IIdentifier identifier)
		{

		}
	}
}