using System;
using SimpleIoC.Bindings.Abstractions;
using SimpleIoC.Identifiers.Abstractions;
using SimpleIoC.Models;

namespace SimpleIoC.Bindings
{
	public sealed class Binding : IBinding
	{
		public Type AbstractType { get; }
		public Type ConcreteType { get; }
		public IIdentifier Identifier { get; }
		public Func<object> InstantiationFunction { get; }
		public ObjectLifetime Lifetime { get; }
		public object Value { get; private set; }


		public Binding(Type abstractType, Type concreteType, IIdentifier identifier = default,
			Func<object> instantiationFunction = default, ObjectLifetime lifetime = default, object value = default)
		{
			AbstractType = abstractType;
			ConcreteType = concreteType;
			Identifier = identifier ?? Defaults.Identifier;
			InstantiationFunction = instantiationFunction;
			Lifetime = lifetime;
			Value = value;
		}


		public void SetValue(object value)
		{
			if (Lifetime != ObjectLifetime.Singleton)
			{
				return;
			}

			Value = value;
		}
	}
}