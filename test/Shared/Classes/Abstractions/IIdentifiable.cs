using System;

namespace SimpleIoC.Tests.Shared.Classes.Abstractions
{
	public interface IIdentifiable
	{
		Guid Identity { get; set; }
	}
}