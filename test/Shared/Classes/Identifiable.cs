using System;
using SimpleIoC.Tests.Shared.Classes.Abstractions;

namespace SimpleIoC.Tests.Shared.Classes
{
	public sealed class Identifiable : IIdentifiable
	{
		public Guid Identity { get; set; } = Guid.NewGuid();
	}
}