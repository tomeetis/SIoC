using System;
using FluentAssertions;
using SimpleIoC.Attributes;
using SimpleIoC.Exceptions;
using SimpleIoC.Identifiers;
using SimpleIoC.Tests.Shared.Classes.Abstractions;
using Xunit;

namespace SimpleIoC.Tests.Integration
{
	public sealed class ContextTests
	{
		private const string AbcIdentifierValue = "ABC";
		private static readonly StringIdentifier AbcIdentifier = AbcIdentifierValue;


		[Fact]
		public void Given_CreatingNewContextWithSingletonFactoryBinding_When_TryingToResolve_ShouldReturnSameValues()
		{
			// Given
			var context = new Context();

			context.Bindings
				.AddSingleton<TestClass1Default>(() => new TestClass1Default())
				.AddSingleton<TestClass2>(() =>
					{
						var testClass = new TestClass2();
						testClass.Identity = Guid.NewGuid();
						return testClass;
					});

			// When
			var testClass1 = context.Get<TestClass1Default>();
			var testClass2 = context.Get<TestClass2>();

			// Should
			testClass1.Identity.Should().Be(testClass2.Identity);
		}

		[Fact]
		public void Given_CreatingNewContextWithTransientFactoryBinding_When_TryingToResolve_ShouldReturnDifferentValues()
		{
			// Given
			var context = new Context();

			context.Bindings
				.AddTransient<TestClass1Default>(() => new TestClass1Default())
				.AddTransient<TestClass2>(() =>
					{
						var testClass = new TestClass2();
						testClass.Identity = Guid.NewGuid();
						return testClass;
					});

			// When
			var testClass1 = context.Get<TestClass1Default>();
			var testClass2 = context.Get<TestClass2>();

			// Should
			testClass1.Identity.Should().NotBe(testClass2.Identity);
		}

		[Fact]
		public void Given_ContextWithSingletonsInDifferentIdentifyingScopes_When_TryingToResolve_ShouldReturnSameValues()
		{
			// Given
			var context = new Context();

			context.Bindings
				.AddSingleton<TestClass1Abc>(() => new TestClass1Abc())
				.AddSingleton<TestClass2>(() =>
					{
						var testClass = new TestClass2();
						testClass.Identity = Guid.NewGuid();
						return testClass;
					}, AbcIdentifier);


			// When
			var testClass1 = context.Get<TestClass1Abc>();
			var testClass2 = context.Get<TestClass2>(AbcIdentifier);

			// Should
			testClass1.Identity.Should().Be(testClass2.Identity);
		}

		[Fact]
		public void Given_ContextHasCircularDependency_When_TryingToResolve_ShouldThrowException()
		{
			// Given
			var context = new Context();

			context.Bindings
				.AddSingleton<CircularDependency1>()
				.AddSingleton<CircularDependency2>();

			// When
			Action action = () => context.Get<CircularDependency1>();

			// Should
			action.Should().Throw<BindingCircularDependencyException>();
		}


		private class TestClass1Default : IIdentifiable
		{
			[Injected]
			private TestClass2 testClass2;

			public Guid Identity
			{
				get => testClass2.Identity;
				set => testClass2.Identity = value;
			}
		}

		private class TestClass1Abc : IIdentifiable
		{
			[Injected("ABC")]
			private TestClass2 testClass2;

			public Guid Identity
			{
				get => testClass2.Identity;
				set => testClass2.Identity = value;
			}
		}

		private class TestClass2 : IIdentifiable
		{
			public Guid Identity { get; set; }
		}

		private class CircularDependency1 : IIdentifiable
		{
			[Injected]
			private CircularDependency2 _testClass;

			public Guid Identity
			{
				get => _testClass.Identity;
				set => _testClass.Identity = value;
			}
		}

		private class CircularDependency2 : IIdentifiable
		{
			[Injected]
			private CircularDependency1 _testClass;

			public Guid Identity
			{
				get => _testClass.Identity;
				set => _testClass.Identity = value;
			}
		}
	}
}